# Doom-Emacs configuration

# Quick setup

1. Copy those files in the `~/.doom.d` folder
2. Adjust `init.el` file with your preferences
3. Add/ Remove `hooks` from `config.el`
4. Follow [doom-emacs install](https://github.com/hlissner/doom-emacs#install)

# Tips

* every time you change your `init.el` file, run `~/.emacs.d/bin/doom sync`
* to check missing modules, packages or dependencies use `~/.emacs.d/bin/doom doctor` and follow specific instructions

## Our config

### Languages

* Common LISP
* Emacs LISP
* Go
* Haskell
* JSON
* Javascript
* Markdown
* PHP
* Python
* Ruby
* Rust
* Sh
* Web (Generic module)
* Yaml

### Other packages

* LSP + Company -> autocompletion
* iBuffer -> buffer management
* docker -> docker commands and shortcuts
* magit
* workspace -> project isolated spaces
