# Solargraph configuration

# Prerequisites

* `gem install solargraph`
* `gem install solargraph-rails`

# Quick setup

1. copy `.yml` file inside the root folder.
2. execute `solargraph download-core`
3. execute `yard gems`
4. execute `solargraph bundle`
5. copy `definitions.rb` into `${PROJECT}/config/`
