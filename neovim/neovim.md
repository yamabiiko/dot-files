# Neovim config (general/development)

## Global settings
```vim
set nocompatible        
set completeopt=preview 
set tabstop=4
set shiftwidth=4
set expandtab
```
These standard vim settings will make the tab key insert 4 spaces instead of using a tab character, and 
use preview mode instead of auto-completing when using completion plugins.
```vim
set encoding=utf-8
set nu
set hidden   
set noshowmode
```
We set the encondig to UTF-8 and show the number line. Setting hidden allows you to quit a buffer without saving it. \
We don't need showmode since we will be using vim-airline for status.
```vim
set nobackup
set nowritebackup
set noswapfile
```
Backups, swapfiles and others are disabled. If your system is unstable, you might want to delete those lines, however it can cause problems with servers and cause messy folders.

## Custom settings 
```vim
autocmd Filetype ruby setlocal ts=2 sw=2 
autocmd Filetype slim setlocal ts=2 sw=2
filetype plugin indent on
```
Using tabspaces of 2 on ruby files. (Maybe should use some plugin to manage this, WIP)
```vim
funcion! WinMove(key)
    let t:curwin = winnr()
    exec "wincmd ".a:key
    if (t:curwin == winnr())
        if (match(a:key,'[jk]'))
            wincmd v
        else
            wincmd s
        endif
        exec "wincmd ".a:key
    endif
endfunction

nnoremap <silent> <C-h> :call WinMove('h')<CR>
nnoremap <silent> <C-j> :call WinMove('j')<CR>
nnoremap <silent> <C-k> :call WinMove('k')<CR>
nnoremap <silent> <C-l> :call WinMove('l')<CR>
```
This function and remap lets us use Ctrl + hjkl keys to move windows in neovim
### Key bindings
```
noremap <C-q> :bd<CR>
map <C-n> :NERDTreeToggle<CR>
map ; :Files<CR> 
map ' :Rg<CR>
nmap <leader>ln :noh<CR>
```

- `CTRL+Q` -> Close buffer
- `CTRL+N` -> Open NERDTree
- `\ln` -> Disable highlighting
-  `'` -> Search by matching inside files (using fzf + ripgrep)
- `;` -> Search by matching name of files (using fzf + fd)
## Plugin settings
```vim
call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree' 
Plug 'neoclide/coc.nvim', { 'branch': 'release' } 
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } 
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline' 
Plug 'vim-airline/vim-airline-themes' 
Plug 'jiangmiao/auto-pairs' " Brackets management plugins
Plug 'machakann/vim-sandwich'
Plug 'airblade/vim-gitgutter' " Git plugin
call plug#end()
```
We use vim-plug for plugin management. More information on how to set it up on its [GitHub repo](https://github.com/junegunn/vim-plug) 
#### Plugin list:

- [NERDTree](https://github.com/preservim/nerdtree) for browsing directories as trees
- [ConquerOfCompletion](https://github.com/neoclide/coc.nvim) as a completion engine (compatible with VSCode ones)
- [fzf](https://github.com/junegunn/fzf) as a fuzzy file finder
- [vim-airline](https://github.com/vim-airline/vim-airline) as a status line 
- [vim-gitgutter](https://github.com/airblade/vim-gitgutter) minimalist git plugin
- [vim-sandwich](https://github.com/machakann/vim-sandwich) and [auto-pairs](https://github.com/jiangmiao/auto-pairs) for brackets management
### Vim-airline settings
```vim
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='term'
let g:airline#extensions#whitespace#enabled = 0 
```
Enable powerline fonts 
### Fzf settings/tips
In order to use fd (fast rust alternative to find) with Fzf, install `fd` and add this to your `.bashrc`:
```
export FZF_DEFAULT_COMMAND='fd --type f --hidden --exclude .git --exclude .vim'
```
To use ripgrep, simply install it and use `:Rg` (or the shortcut `'`) 
### CoC (ConquerOfCompletion) settings/tips
In order to install a particular langauge completion, use `:CocInstall` \
Here is a small list:
- `coc-go` Golang completion
- `coc-rls` Rust completion 
- `coc-pyright` Python completion and static analyzer
- `coc-sh` Bash completion
