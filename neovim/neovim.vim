" Global settings
set nocompatible
set completeopt=preview

set tabstop=4
set shiftwidth=4
set expandtab

set encoding=utf-8
set nu
set hidden   
set noshowmode
set nobackup
set nowritebackup
set noswapfile


" Custom settings and bindings 
autocmd Filetype ruby setlocal ts=2 sw=2 
autocmd Filetype slim setlocal ts=2 sw=2
filetype plugin indent on

noremap <C-q> :bd<CR>
map <C-n> :NERDTreeToggle<CR>
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
map ; :Files<CR> 
map ' :Rg<CR>
nmap <leader>ln :noh<CR>

function! WinMove(key)
    let t:curwin = winnr()
    exec "wincmd ".a:key
    if (t:curwin == winnr())
        if (match(a:key,'[jk]'))
            wincmd v
        else
            wincmd s
        endif
        exec "wincmd ".a:key
    endif
endfunction

nnoremap <silent> <C-h> :call WinMove('h')<CR>
nnoremap <silent> <C-j> :call WinMove('j')<CR>
nnoremap <silent> <C-k> :call WinMove('k')<CR>
nnoremap <silent> <C-l> :call WinMove('l')<CR>


" Plugins
call plug#begin('~/.vim/plugged')

Plug 'preservim/nerdtree' " NerdTree for browsing directories
Plug 'neoclide/coc.nvim', { 'branch': 'release' } " CoC for completion, works similar to VSCode
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " Fuzzy finder 
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline' " Status/tabline for vim
Plug 'vim-airline/vim-airline-themes' 
Plug 'jiangmiao/auto-pairs' " Brackets management plugins
Plug 'machakann/vim-sandwich'
Plug 'airblade/vim-gitgutter' " Git plugin

call plug#end()

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='term'
let g:airline#extensions#whitespace#enabled = 0 
