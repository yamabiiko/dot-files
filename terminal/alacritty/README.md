# Alacritty configuration

## Installation

Copy and rename `alacritty.yml` in your $HOME: `cp alacritty.yml ~/.alacritty.yml`

## Features

* Anonymice Font Mono
* Tomorrow Night Black Theme 
* Shortcuts PopOS Cosmic Compatibles
