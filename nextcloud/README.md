# Nextcloud config

## Features

* S3 enabled
* Redis Cache
* URL Overwriting
* SMTP enable + TLS
* MySQL utf8 enabled
